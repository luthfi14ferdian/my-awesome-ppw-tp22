from django.conf.urls import url
from .views import index
from .auth import getLoginInfos, logout

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'get-login-infos/$', getLoginInfos, name='get-login-infos'),
	url(r'logout/$', logout, name='logout'),
]