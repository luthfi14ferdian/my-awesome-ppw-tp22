from django.db import models
from django.core.validators import URLValidator

# Create your models here.
class AccountProfile(models.Model):
    company_name = models.CharField(max_length=30)
    company_about = models.CharField(max_length=500)
    company_type = models.CharField(max_length=100)
    company_web = models.TextField(validators=[URLValidator()])
    company_specialization = models.CharField(max_length=140)

    def __str__(self):
    	return self.company_name