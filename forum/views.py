from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import Message_Form
from .models import Message
from .utils import *

# Create your views here.
response = {}
def index(request):
    # print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('forum:dashboard'))
    else:
        response['author'] = get_data_user(request, 'user_login')
        html = 'forum/login.html'
        return render(request, html, response)

def dashboard(request):

    if not 'user_login' in request.session.keys():
        return HttpResponseRedirect(reverse('forum:index'))
    else:
        kode_identitas = get_data_user(request, 'kode_identitas')
        try:
            user = User.objects.get(kode_identitas = kode_identitas)
        except Exception as e:
            user = create_new_user(request)

        message = Message.objects.filter(user=user)

        response['message'] = message
        response['message_form'] = Message_Form
        response["message_list"] = message
        html = 'forum/forum.html'

        message_list = message
        paginator = Paginator(message_list, 5)
        page = request.GET.get('page', 1)
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            users = paginator.page(1)
        except EmptyPage:
            users = paginator.page(paginator.num_pages)

        response["message_list"] = users
        html = 'forum/forum.html'
        return render(request, html, response)

def add_message(request, id):
    response['id'] = id
    form = Message_Form(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        kode_identitas = get_data_user(request, 'kode_identitas')
        try:
            user = User.objects.get(kode_identitas = kode_identitas)
        except Exception as e:
            user = create_new_user(request)

        response['title'] = request.POST['title']
        response['message'] = request.POST['message']
        message = Message(user=user,title=response['title'], message=response['message'], kode_message = id)
        message.save()
        response['id'] = id
        html ='forum/forum.html'
        return HttpResponseRedirect('/forum/')

def clear(request,object_id):
    clears = Message.objects.get(pk=object_id)
    clears.delete()
    return HttpResponseRedirect('/forum/')
